<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Segment;
use AppBundle\Form\SegmentType;

/**
 * Segment controller.
 *
 */
class SegmentController extends Controller
{
    /**
     * Lists all Segment entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $segments = $em->getRepository('AppBundle:Segment')->findAll();

        return $this->render('segment/index.html.twig', array(
            'segments' => $segments,
        ));
    }

    /**
     * Creates a new Segment entity.
     *
     */
    public function newAction(Request $request)
    {
        $choices = $this->get('app.manager')->getCategoryNamesAsChoice();

        if (count($choices) === 0) {
            return $this->render('admin/error-page.html.twig', array(
                'error' => 'Registre uma categoria antes de inserir um produto!',
                'link' => '/admin/category/',
            ));
        }

        $segment = new Segment();
        $form = $this->createForm('AppBundle\Form\SegmentType', $segment, ['choices' => $choices]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($segment);
            $em->flush();

            return $this->redirectToRoute('admin_segment_show', array('id' => $segment->getId()));
        }

        return $this->render('segment/new.html.twig', array(
            'segment' => $segment,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Segment entity.
     *
     */
    public function showAction(Segment $segment)
    {
        $deleteForm = $this->createDeleteForm($segment);

        return $this->render('segment/show.html.twig', array(
            'segment' => $segment,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Segment entity.
     *
     */
    public function editAction(Request $request, Segment $segment)
    {
        $choices = $this->get('app.manager')->getCategoryNamesAsChoice();

        if (count($choices) === 0) {
            return $this->render('admin/error-page.html.twig', array(
                'error' => 'Registre uma categoria antes de inserir um produto!',
                'link' => '/admin/category/',
            ));
        }

        $deleteForm = $this->createDeleteForm($segment);
        $editForm = $this->createForm('AppBundle\Form\SegmentType', $segment, ['choices' => $choices]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($segment);
            $em->flush();

            return $this->redirectToRoute('admin_segment_edit', array('id' => $segment->getId()));
        }

        return $this->render('segment/edit.html.twig', array(
            'segment' => $segment,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Segment entity.
     *
     */
    public function deleteAction(Request $request, Segment $segment)
    {
        $form = $this->createDeleteForm($segment);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($segment);
            $em->flush();
        }

        return $this->redirectToRoute('admin_segment_index');
    }

    /**
     * Creates a form to delete a Segment entity.
     *
     * @param Segment $segment The Segment entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Segment $segment)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_segment_delete', array('id' => $segment->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
