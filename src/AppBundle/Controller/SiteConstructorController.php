<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\SiteConstructor;
use AppBundle\Form\SiteConstructorType;

/**
 * SiteConstructor controller.
 *
 */
class SiteConstructorController extends Controller
{
    /**
     * Lists all SiteConstructor entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $siteConstructors = $em->getRepository('AppBundle:SiteConstructor')->findAll();

        return $this->render('siteconstructor/index.html.twig', array(
            'siteConstructors' => $siteConstructors,
        ));
    }

    /**
     * Creates a new SiteConstructor entity.
     *
     */
    public function newAction(Request $request)
    {
        $siteConstructor = new SiteConstructor();
        $form = $this->createForm('AppBundle\Form\SiteConstructorType', $siteConstructor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($siteConstructor);
            $em->flush();

            return $this->redirectToRoute('admin_siteconstructor_show', array('id' => $siteConstructor->getId()));
        }

        return $this->render('siteconstructor/new.html.twig', array(
            'siteConstructor' => $siteConstructor,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a SiteConstructor entity.
     *
     */
    public function showAction(SiteConstructor $siteConstructor)
    {
        $deleteForm = $this->createDeleteForm($siteConstructor);

        return $this->render('siteconstructor/show.html.twig', array(
            'siteConstructor' => $siteConstructor,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing SiteConstructor entity.
     *
     */
    public function editAction(Request $request, SiteConstructor $siteConstructor)
    {
        // echo "<pre>";
        // print_r($siteConstructor->getContentValue());
        // echo "</pre>";
        // die();
        $deleteForm = $this->createDeleteForm($siteConstructor);
        $editForm = $this->createForm('AppBundle\Form\SiteConstructorType', $siteConstructor);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($siteConstructor);
            $em->flush();

            return $this->redirectToRoute('admin_siteconstructor_edit', array('id' => $siteConstructor->getId()));
        }

        return $this->render('siteconstructor/edit.html.twig', array(
            'siteConstructor' => $siteConstructor,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a SiteConstructor entity.
     *
     */
    public function deleteAction(Request $request, SiteConstructor $siteConstructor)
    {
        $form = $this->createDeleteForm($siteConstructor);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($siteConstructor);
            $em->flush();
        }

        return $this->redirectToRoute('admin_siteconstructor_index');
    }

    /**
     * Creates a form to delete a SiteConstructor entity.
     *
     * @param SiteConstructor $siteConstructor The SiteConstructor entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(SiteConstructor $siteConstructor)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_siteconstructor_delete', array('id' => $siteConstructor->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
