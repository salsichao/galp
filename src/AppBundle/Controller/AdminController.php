<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class AdminController extends Controller
{
    public function indexAction()
    {
        $manager = $this->get('app.manager');

        return $this->render('admin/index.html.twig', [
            'content' => $manager->getAdminHome(),
        ]);
    }

    public function helpAction()
    {
    	return $this->render('admin/help.html.twig');
    }
}
