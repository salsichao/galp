<?php

namespace AppBundle\Controller;

use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;

use AppBundle\Entity\Contact;
use AppBundle\Form\ContactType;

class DefaultController extends Controller
{
    public function indexAction(Request $request)
    {
        $manager = $this->get('app.manager');

        return $this->render('site/index.html.twig', [
            'menu' => $manager->getMenu(),
            'adresses' => $manager->getAdresses(),
            'phones' => $manager->getPhones(),
            'emails' => $manager->getEmails(),
            'social' => $manager->getSocialMedia(),
            'slides' => $manager->getSlides(),
            'content' => $manager->getHomeContent(),
        ]);
    }

    public function catalogAction()
    {
        $manager = $this->get('app.manager');

        return $this->render('site/catalog.html.twig', [
            'menu' => $manager->getMenu(),
            'adresses' => $manager->getAdresses(),
            'phones' => $manager->getPhones(),
            'emails' => $manager->getEmails(),
            'social' => $manager->getSocialMedia(),
            'categories' => $manager->getCategoryContent(),
            'segments' => $manager->getSegmentsContent(),
        ]);
    }

    public function catalogFromCategoryAction($category)
    {
        $manager = $this->get('app.manager');

        return $this->render('site/category.html.twig', [
            'menu' => $manager->getMenu(),
            'adresses' => $manager->getAdresses(),
            'phones' => $manager->getPhones(),
            'emails' => $manager->getEmails(),
            'social' => $manager->getSocialMedia(),
            'category' => $manager->getCategoryContent($category),
            'segments' => $manager->getSegmentsFromCategoryUrl($category),
        ]);
    }

    public function catalogFromCategoryAndSegmentAction($category, $segment)
    {
        $manager = $this->get('app.manager');

        return $this->render('site/segments.html.twig', [
            'menu' => $manager->getMenu(),
            'adresses' => $manager->getAdresses(),
            'phones' => $manager->getPhones(),
            'emails' => $manager->getEmails(),
            'social' => $manager->getSocialMedia(),
            'category' => $manager->getCategoryContent($category),
            'segments' => $manager->getSegmentsFromCategoryUrl($category),
            'this_segment' => $segment,
            'products' => $manager->getCatalogContent($segment),
        ]);
    }

    public function productDetailAction($id)
    {
        $manager = $this->get('app.manager');

        return $this->render('site/product.html.twig', [
            'menu' => $manager->getMenu(),
            'adresses' => $manager->getAdresses(),
            'phones' => $manager->getPhones(),
            'emails' => $manager->getEmails(),
            'social' => $manager->getSocialMedia(),
            'product' => $manager->getProductByUrl($id),
        ]);
    }

    public function aboutAction()
    {
        $manager = $this->get('app.manager');
                
        return $this->render('site/about.html.twig', [
            'menu' => $manager->getMenu(),
            'adresses' => $manager->getAdresses(),
            'phones' => $manager->getPhones(),
            'emails' => $manager->getEmails(),
            'social' => $manager->getSocialMedia(),
            'content' => $manager->getAboutContent(),
        ]);
    }

    public function partnersAction()
    {
        $manager = $this->get('app.manager');
                
        return $this->render('site/partners.html.twig', [
            'menu' => $manager->getMenu(),
            'adresses' => $manager->getAdresses(),
            'phones' => $manager->getPhones(),
            'emails' => $manager->getEmails(),
            'social' => $manager->getSocialMedia(),
            // 'content' => $manager->getAboutContent(),
        ]);
    }

    public function contactAction(Request $request)
    {
        $manager = $this->get('app.manager');

        $contact = new Contact();
        $form = $this->createForm('AppBundle\Form\ContactType', $contact);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $contact->setDate(new \DateTime('now'));
            $em = $this->getDoctrine()->getManager();
            $em->persist($contact);
            $em->flush();

            return $this->redirectToRoute('contact');
        }

        return $this->render('site/contact.html.twig', array(
            'menu' => $manager->getMenu(),
            'adresses' => $manager->getAdresses(),
            'phones' => $manager->getPhones(),
            'emails' => $manager->getEmails(),
            'social' => $manager->getSocialMedia(),
            'form' => $form->createView(),
        ));

    }
}
