<?php

namespace AppBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use AppBundle\Entity\Product;
use AppBundle\Form\ProductType;

use AppBundle\Manager\MainManager;

/**
 * Product controller.
 *
 */
class ProductController extends Controller
{
    /**
     * Lists all Product entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $products = $em->getRepository('AppBundle:Product')->findAll();

        return $this->render('product/index.html.twig', array(
            'products' => $products,
        ));
    }

    /**
     * Creates a new Product entity.
     *
     */
    public function newAction(Request $request)
    {
        $choices = $this->get('app.manager')->getSegmentNamesAsChoice();

        if (count($choices) === 0) {
            return $this->render('admin/error-page.html.twig', array(
                'error' => 'Registre uma subcategoria antes de inserir um produto!',
                'link' => '/admin/segments/',
            ));
        }

        $product = new Product();
        $form = $this->createForm('AppBundle\Form\ProductType', $product, ['choices' => $choices]);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $files = $request->files->all()['product']['image'];

            $imageCollection = '[';
            foreach ($files as $file) {
                $fileName = md5(uniqid()).'.'.$file->guessExtension();
                $file->move(
                    $this->container->getParameter('product_directory'),
                    $fileName
                );
                $imageCollection .= '"' . $fileName . '",';
            }
            $imageCollection = rtrim($imageCollection, ',');
            $imageCollection = $imageCollection . ']';

            $product->setImage($imageCollection);

            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('admin_product_show', array('id' => $product->getId()));
        }

        return $this->render('product/new.html.twig', array(
            'product' => $product,
            'form' => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Product entity.
     *
     */
    public function showAction(Product $product)
    {
        $deleteForm = $this->createDeleteForm($product);

        return $this->render('product/show.html.twig', array(
            'product' => $product,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Product entity.
     *
     */
    public function editAction(Request $request, Product $product)
    {
        $choices = $this->get('app.manager')->getSegmentNamesAsChoice();

        if (count($choices) === 0) {
            return $this->render('admin/error-page.html.twig', array(
                'error' => 'Registre uma subcategoria antes de inserir um produto!',
                'link' => '/admin/segments/',
            ));
        }

        $deleteForm = $this->createDeleteForm($product);
        $editForm = $this->createForm('AppBundle\Form\ProductType', $product, ['choices' => $choices]);
        $editForm->handleRequest($request);

        if ($editForm->isSubmitted() && $editForm->isValid()) {
            $images = $request->files->all()['product']['image'];
            
            if (is_array($images) && !is_null($images[0])) {
                $imageCollection = '[';
                foreach ($images as $image) {
                    $fileName = md5(uniqid()).'.'.$image->guessExtension();
                    $image->move(
                        $this->container->getParameter('product_directory'),
                        $fileName
                    );
                    $imageCollection .= '"' . $fileName . '",';
                }
                $imageCollection = rtrim($imageCollection, ',') . ']';
                $product->setImage($imageCollection);
            }
            $em = $this->getDoctrine()->getManager();
            $em->persist($product);
            $em->flush();

            return $this->redirectToRoute('admin_product_edit', array('id' => $product->getId()));
        }

        return $this->render('product/edit.html.twig', array(
            'product' => $product,
            'edit_form' => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Product entity.
     *
     */
    public function deleteAction(Request $request, Product $product)
    {
        $form = $this->createDeleteForm($product);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->remove($product);
            $em->flush();
        }

        return $this->redirectToRoute('admin_product_index');
    }

    /**
     * Creates a form to delete a Product entity.
     *
     * @param Product $product The Product entity
     *
     * @return \Symfony\Component\Form\Form The form
     */
    private function createDeleteForm(Product $product)
    {
        return $this->createFormBuilder()
            ->setAction($this->generateUrl('admin_product_delete', array('id' => $product->getId())))
            ->setMethod('DELETE')
            ->getForm()
        ;
    }
}
