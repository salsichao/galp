<?php

namespace AppBundle\Entity;

/**
 * SiteConstructor
 */
class SiteConstructor
{
    /**
     * @var int
     */
    private $id;

    /**
     * @var string
     */
    private $contentKey;

    /**
     * @var string
     */
    private $contentValue;


    /**
     * Get id
     *
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set contentKey
     *
     * @param string $contentKey
     *
     * @return SiteConstructor
     */
    public function setContentKey($contentKey)
    {
        $this->contentKey = $contentKey;

        return $this;
    }

    /**
     * Get contentKey
     *
     * @return string
     */
    public function getContentKey()
    {
        return $this->contentKey;
    }

    /**
     * Set contentValue
     *
     * @param string $contentValue
     *
     * @return SiteConstructor
     */
    public function setContentValue($contentValue)
    {
        $this->contentValue = $contentValue;

        return $this;
    }

    /**
     * Get contentValue
     *
     * @return string
     */
    public function getContentValue()
    {
        return $this->contentValue;
    }
}

