<?php

namespace AppBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\Extension\Core\Type\TextareaType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\HiddenType;

class ContactType extends AbstractType
{
    /**
     * @param FormBuilderInterface $builder
     * @param array $options
     */
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $now = new \DateTime('now');
        $builder
			->add('name', TextType::class, array('label' => 'Nome'))
            ->add('category', ChoiceType::class, array(
                'label' => 'Categoria',
                'choices' => [
                    'Cliente' => 'Cliente',
                    'Parceria' => 'Parceria',
                    'Outros' => 'Outros Motivos',
                ],
            ))
			->add('email', TextType::class, array('label' => 'Email'))
			->add('message', TextareaType::class, array('label' => 'Mensagem'))
            ->add('date', HiddenType::class, [
                    'data' => $now->format('d/m/Y'),
                ]
            )
			->add('send', SubmitType::class, array('label' => 'Enviar'));
    }
    
    /**
     * @param OptionsResolver $resolver
     */
    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults(array(
            // 'data_class' => 'AppBundle\Entity\Category'
        ));
    }
}
