<?php

namespace AppBundle\Manager;

use Doctrine\ORM\EntityManager;

class MainManager
{

    private $em;

    public function __construct(EntityManager $entityManager)
    {
        $this->em = $entityManager;
    }

    public function getFromRepository($repository)
    {
        return $this->em->getRepository($repository);
    }

    public function getAllFromRepository($repository)
    {
        return $this->getFromRepository($repository)->findAll();
    }

    public function getContent($argument)
    {
        return $this->getFromRepository('AppBundle:SiteConstructor')
            ->findOneByContentKey($argument)
            ->getContentValue();
    }

    public function getSegmentsFromCategoryUrl($categoryUrl)
    {
        $categoryNameByUrl = $this->getCategoryContent($categoryUrl)->getName();

        return $this->getFromRepository('AppBundle:Segment')
            ->findByCategory($categoryNameByUrl);
    }

    public function getMenu()
    {
        $crudeMenu = $this->getAllFromRepository('AppBundle:Menu');
        $responseMenu = [];
        foreach ($crudeMenu as $index) {
            $responseMenu[] = [
                'name' => $index->getName(),
                'link' => $index->getLink(),
                'title' => $index->getTitle(),
                'alt' => $index->getAlt()
            ];
        }
        return $responseMenu;
    }

    public function getSlides()
    {
        $databaseSlides = $this->getFromRepository('AppBundle:Slider')->findAll();


        $slides = [];
        foreach ($databaseSlides as $slide) {
            $slides[] = [
                'src' => $slide->getSource(),
                'link' => $slide->getLink(),
                'desc' => $slide->getDescription(),
            ];
        }

        return $slides;
    }

    public function getHomeContent()
    {
        return $this->getContent('homeContent');
    }

    public function getAboutContent()
    {
        return $this->getContent('aboutContent');
    }

    public function getCatalogContent($segment = null)
    {
        if ($segment == null) {
            return $this->equalizeProducts(
                $this->getAllFromRepository('AppBundle:Product')
            );
        }
        $segment = $this->getSegmentsContent($segment)->getName();
        return $this->equalizeProducts(
            $this->getFromRepository('AppBundle:Product')->findBySegment($segment)
        );
    }

    public function getProductByUrl($url)
    {
        $product = $this->getFromRepository('AppBundle:Product')->findOneByLink($url);
        $equalizedProduct = $this->transformToPhp($product->getImage());
        $product->setImage($equalizedProduct);
        $product->setDetail(
            $this->transformToPhp('[' . $product->getDetail() . ']')
        );
        return $product;
    }

    public function getCategoryContent($category = null)
    {
        if ($category == null) {
            return $this->getAllFromRepository('AppBundle:Category');
        }

        return $this->getFromRepository('AppBundle:Category')->findOneByLink($category);
    }


    public function getSegmentsContent($segment = null)
    {
        if ($segment == null) {
            return $this->getAllFromRepository('AppBundle:Segment');
        }

        return $this->getFromRepository('AppBundle:Segment')->findOneByLink($segment);
    }

    public function getCategoryNamesAsChoice()
    {
        $queryResult = $this->getFromRepository('AppBundle:Category')->findAll();
        $responseChoices = [];
        foreach ($queryResult as $category) {
            $responseChoices[$category->getName()] = $category->getName();
        }
        return $responseChoices;
    }

    public function getSegmentNamesAsChoice()
    {
        $queryResult = $this->getFromRepository('AppBundle:Segment')->findAll();
        $responseChoices = [];
        foreach ($queryResult as $segment) {
            $responseChoices[$segment->getName()] = $segment->getName();
        }
        return $responseChoices;
    }

    public function getPhones()
    {
        return $this->transformToPhp('[' . $this->getContent('phones') . ']');
    }

    public function getEmails()
    {
        return $this->transformToPhp('[' . $this->getContent('emails') . ']');
    }

    public function getAdresses()
    {
        return $this->transformToPhp($this->getContent('addresses'));
    }

    public function getSocialMedia()
    {
        return $this->getContent('social-media');
    }

    public function getAdminHome()
    {
        return [
            'menu' => '/admin/menu',
            'product' => '/admin/product',
            'category' => '/admin/category',
            'content' => '/admin/siteconstructor',
            'segments' => '/admin/segments',
            'slider' =>  '/admin/slider',
            'contact' => '/admin/contact',
        ];
    }

    public function transformToPhp($stringCode){
        $stringTrim = rtrim($stringCode, ',');
        return eval("return " . $stringTrim . ";");
    }

    public function equalizeProducts($products){
        foreach ($products as $product) {
            $images = $this->transformToPhp($product->getImage());
            $product->setImage($images);
        }
        return $products;
    }
}