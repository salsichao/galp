<?php

use AppBundle/Entities;

class generateFixturesData
{
	public function getSampleMenu()
	{
		return new Menu('1', 'Primary Menu');
	}

	public function getSampleSubmenu()
	{
		return new Submenu('1', '11', 'Contact', 'See how enter on Contact with they', '/contact');
	}
}